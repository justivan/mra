package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testPlanetShouldContainObstacle() throws MarsRoverException {

		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 12, obstacles);
		
		assertTrue(rover.planetContainsObstacleAt(7, 8));
	
	}
	
	@Test
	public void testRoverShouldStartAtZeroZeroNorth() throws MarsRoverException {

		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		MarsRover r = new MarsRover(10, 12, obstacles);
		
		assertEquals("(0,0,N)", r.executeCommand(""));
	
	}
	
	@Test
	public void testRoverShouldTurnRight() throws MarsRoverException {

		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		MarsRover r = new MarsRover(10, 12, obstacles);
		
		assertEquals("(0,0,E)", r.executeCommand("r"));
	
	}
	
	@Test
	public void testRoverShouldTurnLeft() throws MarsRoverException {

		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		MarsRover r = new MarsRover(10, 12, obstacles);
		
		assertEquals("(0,0,W)", r.executeCommand("l"));
	
	}
	
	@Test
	public void testRoverShouldMoveForwardFacingNorth() throws MarsRoverException {

		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		MarsRover r = new MarsRover(10, 12, obstacles);
		
		r.setRoverX(7);
		r.setRoverY(6);
		
		assertEquals("(7,7,N)", r.executeCommand("f"));
	
	}
	
	@Test
	public void testRoverShouldMoveForwardFacingWest() throws MarsRoverException {

		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		MarsRover r = new MarsRover(10, 12, obstacles);
		
		r.setRoverX(7);
		r.setRoverY(6);
		r.changeRoverDirection("l");
		
		assertEquals("(6,6,W)", r.executeCommand("f"));
	
	}
	
	@Test
	public void testRoverShouldMoveBackwardFacingNorth() throws MarsRoverException {

		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		MarsRover r = new MarsRover(10, 12, obstacles);
		
		r.setRoverX(7);
		r.setRoverY(6);
		
		assertEquals("(7,5,N)", r.executeCommand("b"));
	
	}
	
	@Test
	public void testRoverShouldMoveBackwardFacingEast() throws MarsRoverException {

		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		MarsRover r = new MarsRover(10, 12, obstacles);
		
		r.setRoverX(1);
		r.setRoverY(1);
		r.changeRoverDirection("r");
		
		assertEquals("(0,1,E)", r.executeCommand("b"));
	
	}
	
	@Test
	public void testRoverShouldMoveWithCombinationOfCommandsFacingEastEnd() throws MarsRoverException {

		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		MarsRover r = new MarsRover(10, 12, obstacles);
		
		assertEquals("(2,2,E)", r.executeCommand("ffrff"));
	
	}
	
	@Test
	public void testRoverShouldMoveWithCombinationOfCommandsFacingWestEnd() throws MarsRoverException {

		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		MarsRover r = new MarsRover(10, 12, obstacles);
		
		r.setRoverX(2);
		r.setRoverY(2);
		
		assertEquals("(0,4,W)", r.executeCommand("fflff"));
	
	}
	
	@Test
	public void testRoverShouldMoveBackwardAroundTheEdge() throws MarsRoverException {
		
		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		MarsRover r = new MarsRover(10, 12, obstacles);

		assertEquals("(0,11,N)", r.executeCommand("b"));
	
	}
	
	@Test
	public void testRoverShouldMoveForwardAroundTheEdgeEst() throws MarsRoverException {
		
		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		MarsRover r = new MarsRover(10, 12, obstacles);

		r.setRoverX(9);
		r.setRoverDirection("E");
		
		assertEquals("(0,0,E)", r.executeCommand("f"));
	
	}
	
	@Test
	public void testRoverShouldMoveForwardAroundTheEdgeWest() throws MarsRoverException {
		
		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		MarsRover r = new MarsRover(10, 12, obstacles);

		r.setRoverX(0);
		r.setRoverDirection("W");
		
		assertEquals("(9,0,W)", r.executeCommand("f"));
	
	}
	
	@Test
	public void testRoverShouldMoveForwardAndEncounterAnObstacle() throws MarsRoverException {
		
		List<String> obstacles = new ArrayList<>();
		obstacles.add("(2,2)");
		obstacles.add("(7,8)");
		MarsRover r = new MarsRover(10, 12, obstacles);

		assertEquals("(1,2,E)(2,2)", r.executeCommand("ffrff"));
	
	}
	
	@Test
	public void testRoverShouldMoveForwardAndEncounterMoreObstacles() throws MarsRoverException {
		
		List<String> obstacles = new ArrayList<>();
		obstacles.add("(0,2)");
		obstacles.add("(1,1)");
		MarsRover r = new MarsRover(10, 12, obstacles);

		assertEquals("(0,0,S)(0,2)(1,1)", r.executeCommand("fffrffrf"));
	
	}
	
	@Test
	public void testRoverShouldMoveBackwardAroundTheEdgeAndObstacle() throws MarsRoverException {
		
		List<String> obstacles = new ArrayList<>();
		obstacles.add("(5,5)");
		obstacles.add("(7,8)");
		obstacles.add("(0,11)");
		MarsRover r = new MarsRover(10, 12, obstacles);

		assertEquals("(0,0,N)(0,11)", r.executeCommand("b"));
	
	}

}
