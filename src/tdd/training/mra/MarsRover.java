package tdd.training.mra;

import java.util.List;

public class MarsRover {
	
	StringBuilder result = new StringBuilder();
	
	private String[] directions = {"N","E","S","W"};
 	private int x = 0;
	private int y = 0;
	private List<String> obstacles = null;
	private String roverDirection = "";
	private int roverX;
	private int roverY;

	public void setRoverX(int newX) {
		roverX = newX;
	}
	
	public void setRoverY(int newY) {
		roverY = newY;
	}
	
	public int getRoverX() {
		return roverX;
	}
	
	public int getRoverY() {
		return roverY;
	}
	
	public String getRoverDirection(){
		return roverDirection;
	}
	
	public void setRoverDirection(String direction) {
		roverDirection = direction;
	}
	
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {

		x = planetX;
		y = planetY;
		roverDirection = directions[0];
		roverX = 0;
		roverY = 0;
		obstacles = planetObstacles;
				
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		
		if( this.obstacles.isEmpty() ) {
			System.out.println("There aren't obstacles on this planet!");
			return false;
		} else {		

			for(int i = 0; i < this.obstacles.size(); i++) {
				
				String[] splitObstacle = obstacles.get(i).split(",");
				
				

				if( splitObstacle[0].contains(String.valueOf(x)) && splitObstacle[1].contains(String.valueOf(y)) ) {
					return true;
				}
				
				
				
			}
			
		}
		
		
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {

		if( commandString.isEmpty() ) {
			
			return "(0,0,N)";
		
		} else if(commandString.length() > 1) {
			
			moveRoverMultipleTimes(commandString);
			
		} else {
	
			checkDirection(commandString.charAt(0));
			
		}
		
		this.result.insert(0, "(" + roverX + "," + roverY + "," + roverDirection + ")" );
		
		return this.result.toString();
	}
	
	
	

	public void turnRover(String command) {
		
		if(command.equals("r") || command.equals("R")) {
			
			changeRoverDirection("r");
			
		} else if(command.equals("l") || command.equals("L")){
			
			changeRoverDirection("l");
			
		}
		
	}
	
	public void changeRoverDirection(String commandDirection) {
		
		if(commandDirection.equals("r")) {
			
			if(roverDirection.equals("N")) {
				roverDirection = directions[1];				
			} else if(roverDirection.equals("E")) {
				roverDirection = directions[2];
			} else if( roverDirection.equals("s")) {
				roverDirection = directions[3];
			} else {
				roverDirection = directions[0];
			}
			
		} else if(commandDirection.equals("l")) {
			
			if(roverDirection.equals("N")) {
				roverDirection = directions[3];				
			} else if(roverDirection.equals("W")) {
				roverDirection = directions[2];
			} else if( roverDirection.equals("s")) {
				roverDirection = directions[1];
			} else {
				roverDirection = directions[0];
			}
		}
		
	}
	
	public void moveForward() throws MarsRoverException {
		
		if(roverIsAtBorderForward()) {
			return;
		} else {
			
			if(roverDirection.equals("N")) {
				
				this.ForwardNorth();
				
			} else if( roverDirection.equals("E") ) {
				
				this.ForwardEast();
				
			} else if( roverDirection.equals("S") ) {
				
				this.ForwardSouth();
				
			} else if( roverDirection.equals("W") ) {
				
				this.ForwardWest();
				
			}
			
		}
		
		
		
		
	}
	
	public void moveBackward() throws MarsRoverException {
		
		if(roverIsAtBorderBackward()) {
			return;
		} else {
			
			if(roverDirection.equals("N")) {
				this.BackWardNorth();
				
			} else if( roverDirection.equals("E") ) {
				this.BackWardEast();
				
			} else if( roverDirection.equals("S") ) {
				this.BackWardSouth();
				
			} else if( roverDirection.equals("W") ) {
				this.BackWardWest();
				
			}
			
		}
	
	}
	
	public void moveRoverMultipleTimes(String commands) throws MarsRoverException {
			
		int i = 0;
		
		while(i<commands.length()) {
			
			checkDirection(commands.charAt(i));
	
			i++;
		}


	}
	
	public void checkDirection(char c) throws MarsRoverException {
		
		if( c == 'f' ) {
			this.moveForward();			
		} else if( c =='b' ) {
			this.moveBackward();			
		} else if( c == 'r' ) {
			this.changeRoverDirection("r");			
		} else if( c =='l' ) {
			this.changeRoverDirection("l");			
		}
		
	}
	
	public boolean roverIsAtBorderForward() throws MarsRoverException {
		
		if( this.roverY == 0 && this.roverDirection == "S" ) {
			if( planetContainsObstacleAt(this.roverX, this.y-1)) {
				this.appendObstacle(this.roverX, this.y-1);
			} else {
				this.roverY = this.y-1;	
			}
			return true;
		} else if( this.roverY == this.y-1 && this.roverDirection == "N" ) {
			
			if( planetContainsObstacleAt(this.roverX, 0)) {
				this.appendObstacle(this.roverX,0);
			} else {
				this.roverY = 0;
			}
			return true;
		} else if(this.roverX == 0 && this.roverDirection == "W") {
			if( planetContainsObstacleAt(this.x-1, this.roverY)) {
				this.appendObstacle(this.x-1, this.roverY);
			} else {
				this.roverX = this.x-1;
			}
			return true;
		} else if(this.roverX == this.x-1 && this.roverDirection == "E") {
			if( planetContainsObstacleAt(0, this.roverY)) {
				this.appendObstacle(this.x-1, this.roverY);
			} else {
				this.roverX = 0;
			}
			return true;
		}
		
		return false;	
		
	}
	
	public boolean roverIsAtBorderBackward() throws MarsRoverException {
		
		if( this.roverY == 0 && this.roverDirection == "N" ) {
			if( planetContainsObstacleAt(this.roverX, this.y-1)) {
				this.appendObstacle(this.roverX,this.y-1);
			} else {
				this.roverY = this.y-1;	
			}
			return true;
			
		} else if( this.roverY == this.y-1 && this.roverDirection == "S" ) {
			if( planetContainsObstacleAt(this.roverX, 0)) {
				this.appendObstacle(this.roverX,0);
			} else {
				this.roverY = 0;
			}
			return true;
		} else if(this.roverX == 0 && this.roverDirection == "E") {
			if( planetContainsObstacleAt(this.x-1, this.roverY)) {
				this.appendObstacle(this.x-1, this.roverY);
			} else {
				this.roverX = this.x-1;
			}
			
			return true;
		} else if(this.roverX == this.x-1 && this.roverDirection == "W") {
			if( planetContainsObstacleAt(0, this.roverY)) {
				this.appendObstacle(this.x-1, this.roverY);
			} else {
				this.roverX = 0;
			}

			return true;
		}
		
	return false;	
		
	}
	
	public void appendObstacle(int obstacleX, int obstacleY) {
		
		String tmp = "(" + obstacleX + "," + obstacleY + ")";
		
		if( !(result.toString().contains(tmp)) ) {
			this.result.append(tmp);
		}
		
		
		
	}
	
	public void ForwardNorth() throws MarsRoverException {
		
		if(  !(planetContainsObstacleAt(this.roverX, this.roverY+1)) ) {
			this.roverY++;
		} else {
			appendObstacle(this.roverX, this.roverY+1);
		}
		
	}
	
	public void ForwardSouth() throws MarsRoverException {
		
		if(  !(planetContainsObstacleAt(this.roverX, this.roverY-1)) ) {
			this.roverY--;
		} else {
			appendObstacle(this.roverX, this.roverY-1);
		}
		
	}

	public void ForwardEast() throws MarsRoverException {
	
		if(  !(planetContainsObstacleAt(this.roverX+1, this.roverY)) ) {
			this.roverX++;
		} else {
			appendObstacle(this.roverX+1, this.roverY);
		}
	
	}
	
	public void ForwardWest() throws MarsRoverException {
		
		if(  !(planetContainsObstacleAt(this.roverX-1, this.roverY)) ) {
			this.roverX--;
		} else {
			appendObstacle(this.roverX-1, this.roverY);
		}
		
	}
	
	public void BackWardNorth() throws MarsRoverException{
		if(  !(planetContainsObstacleAt(this.roverX, this.roverY-1)) ) {
			this.roverY--;
		} else {
			appendObstacle(this.roverX, this.roverY-1);
		}
	}
	
	public void BackWardSouth() throws MarsRoverException{
		if(  !(planetContainsObstacleAt(this.roverX, this.roverY+1)) ) {
			this.roverY++;
		} else {
			appendObstacle(this.roverX, this.roverY+1);
		}
	}
	
	public void BackWardWest() throws MarsRoverException{
		if(  !(planetContainsObstacleAt(this.roverX+1, this.roverY)) ) {
			this.roverX++;
		} else {
			appendObstacle(this.roverX+1, this.roverY);
		}
	}
	
	public void BackWardEast() throws MarsRoverException{
		if(  !(planetContainsObstacleAt(this.roverX-1, this.roverY)) ) {
			this.roverX--;
		} else {
			appendObstacle(this.roverX-1, this.roverY);
		}
	}


	
}
